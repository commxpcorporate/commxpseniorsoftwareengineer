## Senior Java Software Engineer Work Sample

This work sample will be used to assess your code skills as well as your ability to present and justify your work.

You should send us a **Spring Boot project** than can be built and run fulfilling the scenario below.

You are asked to develop an application that acts as both a client and a server. 

In this repository an application is supplied. You will use this server application to complete the scenario below.

### Scenario

Your application registers on start up to a the server's Web Service (SOAP). A few seconds later it will start to receive DTOs, in JSON format, in a REST endpoint you have registered with the server.

In the JSON recieved you get an ``msisdn`` (which serves as the userId), a ``transactionId`` and a ``balance``. 

You are required to send back to the server, using the Web Service endpoint, an offer based on the balance of the user at a price point of 30% of his balance. 

You must also send the requests at no more than **7 TPS** regardless of how fast the server is sending messages at your REST endpoint.

Any client request that fails for any reason should be retried up to **5 times** and then logged as failed and discarded.

Also clients with balance over **30.0** should be assigned a ``HIGH`` priority and take precedence in sending over others.

### How to run the service

You will need java 8+ in order to run the server jar (included in this repository). 

You can run it via: 

``java -jar server.jar``

The WSDL for the Web Service of the server is at (assuming you are going to run the included server jar on the same machine as your application):

``http://localhost:4221/ws/incomingMessageRequest.wsdl``

The JSON for the incoming messages is:

```
{
    "msisdn": "+30697524624",
    "transactionId": "445566fggwgkg0999",
    "balance": "20.55"
}
```

### What we are looking for

Clean, extensible, scalable and reliable code. Proper error handling, proper logging and proper jastification for your choices. 

Good luck!